# Gitleaks pre-commit hook local repository protection

## Intro

This project's purpose is to instrument a local Git repository with Gitleaks, which is a SAST tool for detecting and preventing hardcoded secrets like passwords, api keys, and tokens in git repos. 

## Getting started

### Prerequisites

- Golang installed (mandatory)
- Git Bash installed (if you are working on Windows OS)

If Golang is not installed in your OS, you can follow [Golang installation guide](https://go.dev/doc/install)

To install Git Bash, please visit the corresponding [Download Page](https://git-scm.com/download/win)

### Pre-commit hook installation

To install the Gitleaks pre-commit hook into your **local** Git repository, just navigate to your repo root directory and run:

```bash
curl -fsSL https://gitlab.com/edelweissman/gitleaks-pre-commit-hook/-/raw/main/pre-commit-installer.sh | sh
```

Now, the Gitleaks pre-commit hook is installed into your **local** Git repository and is Enabled by default. In case you want to disable it, run:
```bash
git config --local --bool hooks.pre-commit.enable false
```
You can enable it again by running this command:
```bash
git config --local --bool hooks.pre-commit.enable true
```

## Usage

After the Gitleaks pre-commit hook is installed, it will automatically track commits in your repository, and if there is secret-containig file among them, commit will be denied and a failure message similar to following will be displayed:

![secret containig commit is denied](./.media/pre-commit-denial-message.png)