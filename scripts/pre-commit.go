package main

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

var gitleaksInstallDirWin = "C:\\Gitleaks"

func main() {
	if gitleaksEnabled() {
		if len(os.Args) < 2 {
			log.Fatal("No repo directory passed")
		}
		// Get repo root dir
		repoRootDir := os.Args[1]
		gitleaksPath := checkGitleaks()
		runGitleaks(gitleaksPath, repoRootDir)
	} else {
		return
	}
}

func gitleaksEnabled() bool {
	// Get pre-commit hook config status
	cmd := exec.Command("git", "config", "--get", "hooks.pre-commit.enable")
	out, err := cmd.Output()
	if err != nil {
		log.Fatal("Error acquiring git config")
	}
	switch strings.TrimSpace(string(out)) {
	case "true":
		return true
	case "false":
		return false
	default:
		log.Fatal("Error acquiring git config")
	}
	return false
}

func gitleaksGetLatestURL(osType string) (string, error) {
	repoURL := "https://api.github.com/repos/gitleaks/gitleaks/tags"

	// Make HTTP request
	resp, err := http.Get(repoURL)
	if err != nil {
		log.Fatal("HTTP request failure: ", err)
	}
	defer resp.Body.Close()

	// Read respond body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Respond body read error: ", err)
	}

	// Unpack JSON
	var tags []struct {
		Name string `json:"name"`
	}

	err = json.Unmarshal(body, &tags)
	if err != nil {
		log.Fatal("JSON parsing error: ", err)
	}

	// Obtain Tag
	if len(tags) > 0 {
		latestTag := tags[0].Name
		// Remove "v" prefix
		latestTagWithoutV := strings.TrimPrefix(latestTag, "v")
		archiveName := fmt.Sprintf("gitleaks_%s_%s", latestTagWithoutV, osType)
		switch osType {
		case "linux":
			return fmt.Sprintf("https://github.com/gitleaks/gitleaks/releases/download/%s/%s_x64.tar.gz", latestTag, archiveName), nil
		case "windows":
			return fmt.Sprintf("https://github.com/gitleaks/gitleaks/releases/download/%s/%s_x64.zip", latestTag, archiveName), nil
		case "darwin":
			return fmt.Sprintf("https://github.com/gitleaks/gitleaks/releases/download/%s/%s_x64.tar.gz", latestTag, archiveName), nil
		default:
			log.Fatal("Unsupported OS: ", osType)
		}
	}

	return "", fmt.Errorf("Tags not found in GitHub API respond from Gitleaks repository")
}

func downloadFile(url, filePath string) error {
	// Extract directory from file path
	dir := filepath.Dir(filePath)

	// Create complete path if it doesn't exist
	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		log.Fatal("Path creation failure: ", err)
	}

	// Make HTTP request
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal("HTTP request failure: ", err)
	}
	defer resp.Body.Close()

	// Create a file in specified directory
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatal("File creation failure: ", err)
	}
	defer file.Close()

	// Copy respond body into a file
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Fatal("Data copying failure: ", err)
	}
	return nil
}

func extractFile(zipFile, destPath, filename string) error {
	// Open ZIP-archive in Read mode
	r, err := zip.OpenReader(zipFile)
	if err != nil {
		log.Fatal("Error opening zip archive: ", err)
	}
	defer r.Close()

	// Search archive for specified filename
	var targetFile *zip.File
	for _, f := range r.File {
		if f.Name == filename {
			targetFile = f
			break
		}
	}

	if targetFile == nil {
		log.Fatal("File not found in archive:", filename)
	}

	// Open target archived file
	src, err := targetFile.Open()
	if err != nil {
		log.Fatal("Error opening target archived file: ", err)
	}
	defer src.Close()

	// Create target directory if doesn't exist
	err = os.MkdirAll(destPath, os.ModePerm)
	if err != nil {
		log.Fatal("Error creating directory: ", err)
	}

	// Create file or open in Write mode
	dstPath := filepath.Join(destPath, filename)
	dst, err := os.Create(dstPath)
	if err != nil {
		log.Fatal("Error creating file: ", err)
	}
	defer dst.Close()

	// Copy file contents from archive into target file
	_, err = io.Copy(dst, src)
	if err != nil {
		log.Fatal("Error copying archive data: ", err)
	}
	return nil
}

func checkGitleaks() string {
	osType := ""
	detectedOs := runtime.GOOS
	if strings.Contains(strings.ToLower(detectedOs), "linux") {
		osType = "linux"
	} else if strings.Contains(strings.ToLower(detectedOs), "windows") {
		osType = "windows"
	} else if strings.Contains(strings.ToLower(detectedOs), "darwin") {
		osType = "darwin"
	} else {
		log.Fatal("Unsupported OS detected")
		return ""
	}
	// Define the gitleaks latest release archive URL for current OS
	gitleaksArchiveURL, err := gitleaksGetLatestURL(osType)
	if err != nil {
		log.Fatal("Error:", err)
	}
	// Check if OS is NIX-type
	if osType == "linux" || osType == "darwin" {
		// Check if "gitleaks" is reachable from PATH
		_, err := exec.LookPath("gitleaks")
		if err == nil {
			// Return "gitleaks" if file is reachable
			log.Printf("Gitleaks found in PATH scope")
			return "gitleaks"
		} else {
			// Install gitleaks for NIX OS:
			downloadFile(gitleaksArchiveURL, "gitleaks-nix-archive.tar.gz")
			gitleaksExtractCmd := "tar -xvzf gitleaks-nix-archive.tar.gz gitleaks && sudo mv gitleaks /usr/local/bin/ && rm gitleaks-nix-archive.tar.gz"
			cmd := exec.Command("sh", "-c", fmt.Sprintf(gitleaksExtractCmd))
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			err = cmd.Run()
			if err != nil {
				log.Fatal("Error during NIX-gitleaks installation: ", err)
			}
			return "gitleaks"
		}

	} else if osType == "windows" {
		gitleaksFullPathWin := filepath.Join(gitleaksInstallDirWin, "gitleaks.exe")
		_, err := os.Stat(gitleaksFullPathWin)
		if err == nil {
			log.Printf("Found gitleaks: %s\n", gitleaksFullPathWin)
			return gitleaksFullPathWin
		}
		// Install gitleaks for Windows OS:
		downloadFile(gitleaksArchiveURL, "gitleaks-windows-archive.zip")
		err = extractFile("gitleaks-windows-archive.zip", gitleaksInstallDirWin, "gitleaks.exe")
		if err != nil {
			log.Fatal("Error during WIN-gitleaks installation: ", err)
		}
		err = os.Remove("gitleaks-windows-archive.zip")
		if err != nil {
			log.Fatal("File deletion error: ", err)
		}
		log.Println("Gitleaks successfully installed into ", gitleaksFullPathWin)
		return gitleaksFullPathWin
	}
	log.Fatal("Unsupported OS detected")
	return ""
}

func runGitleaks(gitleaksPath, repoRootDir string) {
	cmd := exec.Command(gitleaksPath, "protect", "--staged", repoRootDir, "--verbose", "--redact")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatal("Gitleaks check failed: ", err)
	}
}
