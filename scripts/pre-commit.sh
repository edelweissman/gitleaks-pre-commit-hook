#!/bin/bash
# Get the repository root directory
repo_root=$(git rev-parse --show-toplevel)
# Run gitleaks check
go run ${repo_root}/.git-scripts/pre-commit.go "$repo_root"
# Report OK if check is passed
if [ $? -eq 0 ]; then
  echo "Commit successful"
  exit 0
else
  echo "Commit did not pass gitleaks secrets check"
  exit 1
fi