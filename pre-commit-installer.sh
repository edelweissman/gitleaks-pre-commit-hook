#!/bin/bash
# Get current directory
current_dir="$PWD"
# Define git hooks directory
hooks_dir="$current_dir/.git/hooks"
# Define git scripts directory
git_scripts_dir="$current_dir/.git-scripts"
# Download git pre-commit hook script
curl -fsSL https://gitlab.com/edelweissman/gitleaks-pre-commit-hook/-/raw/main/scripts/pre-commit.sh -o "$hooks_dir/pre-commit"
# Make it executable
chmod +x "$hooks_dir/pre-commit"
# Add the option to enable pre-commit checking and set it to true by default
git config --local --add hooks.pre-commit.enable true
# Make a path for Go script
mkdir -p $git_scripts_dir
# Download the main gitleaks utilizing script
curl -fsSL https://gitlab.com/edelweissman/gitleaks-pre-commit-hook/-/raw/main/scripts/pre-commit.go -o "$git_scripts_dir/pre-commit.go"
# Add .git-scripts to .gitignore
# If no ".gitignore" found, then create it and add itself along with scripts folder
if [ ! -f "$current_dir/.gitignore" ]; then
    echo ".gitignore" > "$current_dir/.gitignore"
    echo ".git-scripts/*" >> "$current_dir/.gitignore"
else
    echo ".git-scripts/*" >> .gitignore
fi
# Report all ok
echo "Pre-commit git hook installed"